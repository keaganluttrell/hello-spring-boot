package com.galvanize.keaganluttrell.hellospringboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(HelloController.class)
public class HelloControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void helloSpring_noArgs_returnsAHelloSpringMsg() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/hello"))
                .andExpect(status().isOk())
                .andExpect(content().string(
                        "Hello World from Spring"
                        )
                );
    }

    @Test
    void helloSpring_singleNameArg_returnsHelloNameSpringMsg() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/hello?name=Keagan"))
                .andExpect(status().isOk())
                .andExpect(content().string(
                        "Hello Keagan from Spring"
                        )
                );
    }
}
