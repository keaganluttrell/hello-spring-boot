package com.galvanize.keaganluttrell.hellospringboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VowelCounter {
    @GetMapping("/countVowels")
    public String vowelCount(@RequestParam(required = false, defaultValue = "") String word) {
        String vowels = "aeiou";
        char[] letters = word.toLowerCase().toCharArray();
        int sum = 0;
        for (char letter : letters) {
            if (vowels.contains(String.valueOf(letter))) {
                sum++;
            }
        }
        return String.valueOf(sum);
    }

}
